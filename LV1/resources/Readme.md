Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci. 
Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value). 
Međutim, skripta ima bugove i ne radi kako je zamišljeno.

- linija 3 raw_input promijenjen u input, raw_input se ne koristi u python 3
- linija 5 fnamex ispravljen u fname, potrebno je ispravno predati ime varijable kako bi se mogla koristiti
- linija 7 dodane zagrade za naredbu printf ('File cannot be opened:', fname)
- linija 14 word in line
- linija 18 counts[word] += 1 
- linija 20 print(counts)

 