while True:
    try:
        number = float(input("Unesite broj: ")) 
        break         
    except ValueError: 
        print ("Niste unjeli broj, pokusajte ponovo!")

if number >= 0.9 : #provjeravamo kojoj kategoriji pripada broj
    print ("Uneseni broj" + str(number) + " pripada kategoriji A" )
elif number >= 0.8 and number < 0.9 :
    print ("Uneseni broj " + str(number) +" pripada kategoriji B")
elif number >= 0.7 and number < 0.8 :
    print ("Uneseni broj " + str(number) + " pripada kategoriji C")
elif number >= 0.6 and number < 0.7 :
    print ("Uneseni broj " + str(number) +" pripada kategoriji D")
elif number < 0.6 :
    print ("Uneseni broj " + str(number) + " pripada kategoriji F")