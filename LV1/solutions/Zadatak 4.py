n=[None]*1000   #popunjavamo matrice n i z velicine 1000 sa None vrijednostima
z=[None]*1000
counter = 0     #postavljamo pocetne vrijednosti varijabli na 0
brojac = 0
i = 0
ukupno=0

while True:     
    try:
        n[counter] = input("Unesite broj: ")
        if  n[counter] == "Done":
            print ("Korisnik je unio %d brojeva" % brojac)
            break   
        else:
            z[brojac] = float(n[counter])    
            brojac=brojac + 1       #brojac se povecava za 1 za spremanje novog broja
        
    except ValueError:       #ako unos nije "Done" i nije broj prelazi se u ovaj dio
            counter = counter + 1    #Unos se sprema u n[counter] i counter se povecava za 1
            print ("Niste unjeli broj, pokusajte ponovo!")     #ispis na ekran o ponovom unosu vrijednosti

minimum=z[0]    #varijable minimum i maximum inicijaliziramo na vrijednosti spremljene u z[0]
maximum=z[0]

while i < brojac:       #izvrsava se sve dok ne prodjemo kroz sve spremljene vrijednosti u z matrici
    ukupno = ukupno + z[i]      #racuna zbroj svih elemenata kako bismo mogli izracunati srednju vrijednost

    if z[i+1] != None and z[i+1]<minimum:   #provjerava je li to zadnji element kojeg smo unjeli u matricu
        minimum=z[i+1]      #ako zadovoljava i da je manji od postavljenog minimuma tada je to novi minimum
    else:
        minimum=minimum     #inace minimum ostaje ista vrijednost
        
    if z[i+1] != None and z[i+1]>maximum:       #provjerava je li to zadnji element kojeg smo unjeli u matricu
        maximum=z[i+1]       #ako zadovoljava i da je veci od postavljenog maximuma tada je to novi maximum
    else:
        maximum=maximum     #inace maximum ostaje ista vrijednost
    i += 1                  #povecavamo korak za 1

print ("Minimalna vrijednost koju ste unjeli  je: "+ str(minimum))
print ("Maximalna vrijednost koju ste unjeli  je: "+str(maximum))
print ("Srednja vrijednost brojeva koje ste unjeli  je: %.2f" % (ukupno/brojac))
